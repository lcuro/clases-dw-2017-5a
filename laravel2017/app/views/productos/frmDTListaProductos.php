<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript" src="/js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="/js/datatables.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/css/datatables.min.css">
</head>
<body>

<script type="text/javascript">
	$(document).ready(function() {
    	
    	$('#TProductos').DataTable( {
        	
        	"ajax": "/productos/listarproductos",
        	 "columns": [
            { "data": "id" },
            { "data": "codigo" },
            { "data": "nombre" },
            { "data": "categoria" }
            
        ],

         "columnDefs": [

         { "targets": [4], "visible":true,
         		"render":function(data,type,row) {
         			return "<input type='button' value='Editar'>";
         		}
         },

         { "targets": [5], "visible":true,
                "render":function(data,type,row) {
                    return "<input type='button' value='Eliminar'>";
                }
         },
         { "targets": [1], "visible":true}

         ]
        	
    } );
} );

</script>


<table id="TProductos" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Codigo</th>
                <th>Nombre</th>
                <th>Categoria</th>
                <th width="10px">Editar</th>
                <th width="10px">Eliminar</th>
            </tr>
        </thead>
        
</table>


</body>
</html>