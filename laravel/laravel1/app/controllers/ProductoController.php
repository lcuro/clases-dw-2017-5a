<?php

class ProductoController extends BaseController {



public function getListarproductos()
{

	$listaproductos=Producto::all();

	return View::make("Productos.frmListarProductos")->with("listaproductos",$listaproductos);

}

public function getNuevoproducto()
{

	return View::make("Productos.frmNuevoProducto");
}

public function postGuardarproducto()

{
  	$producto= new Producto();

  	$producto->codigo=Input::get("codigo");
  	$producto->nombre=Input::get("nombre");
  	$producto->categoria=Input::get("categoria");

  	$producto->save();

  	return Redirect::to('/productos/listarproductos');

}

public function postActualizarproducto()
{

}


public function getEditarproducto($idproducto)
{

		$producto= Producto::find($idproducto);

	return View::make("Productos.frmEditarProducto")->with("producto",$producto);	

}

public function getListarproductosajax()
{
  return View::make("productos.frmListarProductosAjax");
}


}
